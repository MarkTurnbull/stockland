require([
  "esri/map",
  "esri/tasks/QueryTask",
  "esri/toolbars/edit",
  "esri/graphic",
	"esri/geometry/Extent",
  "esri/symbols/PictureMarkerSymbol",
  "esri/symbols/SimpleMarkerSymbol",
  "esri/symbols/SimpleFillSymbol",
  "esri/symbols/SimpleLineSymbol",
  "esri/Color",
  "esri/tasks/query",
  "esri/InfoTemplate",
  "esri/geometry/Point",
  "esri/layers/GraphicsLayer",
	"esri/arcgis/Portal",
	"esri/arcgis/utils",
	"esri/dijit/util/busyIndicator",
	"dojo/_base/array",
	"dojo/on",
  "dojo/domReady!"
], function (
  Map, QueryTask, Edit, Graphic, Extent,
  PictureMarkerSymbol, SimpleMarkerSymbol, SimpleFillSymbol, SimpleLineSymbol,
  Color, Query, InfoTemplate,
  Point, GraphicsLayer, arcgisPortal, arcgisUtils, busyIndicator, arrayUtils, on
) {
    //Constants----------------------
    esri.arcgis.utils.arcgisUrl = "http://stockland.maps.arcgis.com/sharing/rest/content/items";
    //var mapid = "313e1aa7cf884ad1bfc54974186da3c7";
    var levelFieldName = "Level_";
    var storeKeyField = "ExternalKey";
    var searchFieldName = "TradingName"; //This field is used in search widget as store name
    var storeLayerName = "DEV_Stockland_CP_Retail_Website_WFL1 - StocklandTenantedStores";
    var centreIdFieldName = "BusinessEntity";
		var extentBuffer=40;
    //var StoreSapId = "2000/20180EA/116";
    //var StoreSapId = "2000/20180EA/137";

    var map;
		var store;
		var storeLayer;
		var highlightLayer;
		var maxExtent;
    
		var portalURL = "https://stockland.maps.arcgis.com";
		var mapName = "DEV_Stockland_CP_Retail_Website";
		
		var lods = [
			{
				"level": 1,
				"resolution": 1.058335,
				"scale": 4000
			},
			{
				"level": 2,
				"resolution": 0.714376,
				"scale": 2700
			},
			{
				"level": 3,
				"resolution": 0.529168,
				"scale": 2001
			},
			{
				"level": 4,
				"resolution": 0.396876,
				"scale": 1500
			},
			{
				"level": 5,
				"resolution": 0.317301,
				"scale": 1200
			},
			{
				"level": 6,
				"resolution": 0.238125,
				"scale": 900
			},
			{
				"level": 7,
				"resolution": 0.185209,
				"scale": 700
			},
			{
				"level": 8,
				"resolution": 0.132292,
				"scale": 500
			},
			{
				"level": 9,
				"resolution": 0.092604,
				"scale": 350
			},
			{
				"level": 10,
				"resolution": 0.066146,
				"scale": 250
			},
			{
				"level": 11,
				"resolution": 0.052917,
				"scale": 200
			}
		];

		
		var biHandle=busyIndicator.create("map");
		biHandle.show();
		
		portal=new arcgisPortal.Portal(portalURL);
		
		on(portal, "load", function(p) {
			portal.queryItems({
				num: 1,
				q: "type:'Web Map' AND title:'" + mapName + "'"
			}).then(function(result) {
				if (result.results.length == 0) {
					console.error("Could not find web map named " + mapName);
					biHandle.hide();
				}
				else {
					console.log("Map with id " + result.results[0].id + " will be used");
					
					var storeLayer;
					
					arcgisUtils.getItem(result.results[0].id).then(function(portalItem) {
						storeLayer = arrayUtils.filter(portalItem.itemData.operationalLayers, function(layer) {
							return layer.title == storeLayerName;
						});

						var query = new Query();
						query.outFields = ['*'];
						query.returnGeometry = true; 
						query.where = storeKeyField + " = '" + StoreSapId + "'";

						var queryTask = new QueryTask(storeLayer[0].url);
						queryTask.execute(query, function (featureset) {
							store = featureset.features[0];
							var storeLevel = store.attributes[levelFieldName];
							var storeExtent = store.geometry.getExtent();
							storeExtent.xmin-=extentBuffer;
							storeExtent.ymin-=extentBuffer;
							storeExtent.xmax+=extentBuffer;
							storeExtent.ymax+=extentBuffer;

							arrayUtils.forEach(portalItem.itemData.bookmarks, function(bookmark) {
								if (bookmark.name==store.attributes[centreIdFieldName]) {
									maxExtent=new Extent(bookmark.extent);
								};
							});
							
							arcgisUtils.createMap(portalItem.item.id, "map", {
								mapOptions: {
									slider: false,
									isClickRecentre: false,
									isDoubleClickZoom: false,
									isPinchZoom: false,
									isRubberBandZoom: false,
									isKeyboardNavigation: false,
									isScrollWheel: false,
									extent: storeExtent,
									logo: false,
									lods: lods
								}
							}).then(function(response) {
								map=response.map;
								highlightLayer = new GraphicsLayer({id:"highlights"});
								map.addLayer(highlightLayer);
                addHighlightingEffect(storeLayer[0].id);
								addMapMarker();
								map.on('extent-change', restrictExtent);
                //map.disableScrollWheelZoom();
								biHandle.hide();
							}, function (error) {
								console.log("Error: ", error.code, " Message: ", error.message);
								biHandle.hide();
							});
								;
						});
					});
				}
			});
		});

		function addMapMarker() {
			var markerSymbolURL="assets/07-map-marker@2x.png";
			var storeMarkerSymbol = new PictureMarkerSymbol(markerSymbolURL,24,39);
			storeMarkerSymbol.setOffset(0,19);
			var storeCentroid = store.geometry.getExtent().getCenter(); //Used getExtent().getCenter() to overcome an issue with getCentroid()
			
			var storeMarkerGraphic = new Graphic(storeCentroid,storeMarkerSymbol);
			map.graphics.add(storeMarkerGraphic);
		};
		
		function addHighlightingEffect(storeLayerId) {
        var storeLayer = map.getLayer(storeLayerId);

        var highlightSymbol = new SimpleFillSymbol(
         SimpleFillSymbol.STYLE_SOLID,
         null,
         new Color([170, 190, 255, 0.35])
        );

        var highlightedStore;
        storeLayer.on("mouse-over", function (evt) {
            highlightLayer.clear();
            map.setMapCursor("pointer");
            var highlightGraphic = new Graphic(evt.graphic.geometry, highlightSymbol);
            highlightLayer.add(highlightGraphic);
            highlightedStore = evt.graphic;
        });

        highlightLayer.on("mouse-out", function (evt) {
            highlightLayer.clear();
            map.setMapCursor("default");
        });

        highlightLayer.on("click", function (evt) {
            if (!highlightedStore)
                return;
            getStoreDetails(highlightedStore.attributes[storeKeyField],
               function (response) {
                   if (response == "") //No responce from the callout
                       response = '{"StoreName": "' + highlightedStore.attributes[searchFieldName] + '" }';
											 
                   var infoTemplate = new InfoTemplate({ anchor: "top" });
                   infoTemplate.setTitle(" ");
                   infoTemplate.setContent(createPopupContent(response));
                   evt.graphic.setInfoTemplate(infoTemplate);
               });
        });
    }


    function createPopupContent(response) {
        var storeInfoArr = JSON.parse(response);
        var content;
        if (Object.keys(storeInfoArr).length == 1) {
            //Showing just the store name
            var storename = storeInfoArr.StoreName;
            content = '<div class="popup">' +
                      '<div class="store-name">' + storename + '</div>' +
                      '</div>';
            return content;
        }

        var storename = storeInfoArr.StoreName;
        var storeUrl = storeInfoArr.StorePageUrl;
        var storeLogo = storeInfoArr.StoreLogoUrl;
        var openingHours = storeInfoArr.OpeningHours;
        var phoneNumber = storeInfoArr.PhoneNumber;
        var level = storeInfoArr.Level;
        var storePageUrl = storeInfoArr.StorePageUrl;

        //Create popup html content
        content = "<a  href =" + "'" + storePageUrl + "'" + ">" +
            '<div class="popup">' +
                      '<div class="store-name">' + storename + '</div>' +
                      '<div class="store-opening-hours">' + openingHours + '</div>' +
                      '<div class="bottom-row">' +
                         '<div class="phone-number">' + phoneNumber + '</div>' +
                         '<div class="store-level">' + level + '</div>' +
                      '</div>' +
                   '</div>' +
            "</a>";

        return content;
    }

    //Test function.
    function GetStoreDetails(sapId, callback) {
        var myjson = '{ "SapId": "111", "StoreName": "Aldi", "StoreLogoUrl": "/~/media/shopping-centre/common/others/2013/11/aldi-logo-resized.ashx", "StorePageUrl": "/shopping-centres/centres/stockland-merrylands/stores/aldi", "OpeningHours": "8:30am - 9:00pm", "PhoneNumber": "13 25 34", "Level": "Ground" }';
        myjson = "";
        callback(myjson);
        return myjson;
    }

    function getStoreDetails(sapId, callback) {
        if (sapId != '') {
            $.ajax({
                url: "/public/map/GetRetailStoreDetails",
                method: "POST",
                contentType: 'application/json; charset=utf-8',
                dataType: 'json',
                data: "{'storeSapId':" + JSON.stringify(sapId) + "}",
                success: function (response) {
                    callback(JSON.stringify(response[0]));
                },
                error: function (exception) {
                    console.log('Could not retrieve Store details : ' + exception);
                    callback("");
                }
            });
        }
    }

		function restrictExtent(event) {
			var extent = map.extent.getCenter();
			if (!maxExtent.contains(map.extent)) {
				var newExtent=map.extent;
				if (newExtent.xmax > maxExtent.xmax) {
					newExtent=map.extent.offset(maxExtent.xmax-newExtent.xmax,0);
				}
				if (newExtent.ymax > maxExtent.ymax) {
					newExtent=map.extent.offset(0,maxExtent.ymax-newExtent.ymax);
				}
				if (newExtent.xmin < maxExtent.xmin) {
					newExtent=map.extent.offset(maxExtent.xmin-newExtent.xmin,0);
				}
				if (newExtent.ymin < maxExtent.ymin) {
					newExtent=map.extent.offset(0,maxExtent.ymin-newExtent.ymin);
				}
				map.setExtent(newExtent);
			}
		}
});

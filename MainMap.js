require([
  "esri/map",
  "esri/tasks/QueryTask",
  "esri/toolbars/edit",
  "esri/graphic",
  "esri/lang",
  "esri/tasks/RelationshipQuery",
  "esri/layers/FeatureLayer",
  "esri/symbols/PictureMarkerSymbol",
  "esri/symbols/SimpleFillSymbol",
	"esri/symbols/SimpleLineSymbol",
  "esri/dijit/editing/Editor",
  "esri/dijit/editing/TemplatePicker",
  "esri/renderers/SimpleRenderer",
  "esri/renderers/ClassBreaksRenderer",
  "esri/config",
  "esri/request",
	"dojo/_base/array",
  "esri/Color",
	"dojo/parser",
  'dojo/_base/lang',
  "esri/tasks/query",
  "esri/InfoTemplate",
  "dojo/dom",
  "dojo/on",
	"dojo/promise/all",
  "esri/dijit/Search",
  "esri/geometry/Point",
  "esri/layers/GraphicsLayer",
  "esri/dijit/BasemapToggle",
  "dijit/TooltipDialog",
  "dijit/popup",
  "dojo/dom-style",
  "dojo/number",
  "esri/dijit/HomeButton",
	"esri/arcgis/Portal",
	"esri/dijit/util/busyIndicator",
  "dojo/domReady!"
], function(
  Map, QueryTask, Edit, Graphic, esriLang, RelationshipQuery,
  FeatureLayer,
  PictureMarkerSymbol, SimpleFillSymbol, SimpleLineSymbol,
  Editor, TemplatePicker, SimpleRenderer, ClassBreaksRenderer,
  esriConfig, esriRequest,
  arrayUtils, Color, parser, lang, Query, InfoTemplate, dom, on, all,
  Search, Point, GraphicsLayer, BasemapToggle, TooltipDialog, dijitPopup, domStyle, number, HomeButton, arcgisPortal, busyIndicator
) {

    esri.arcgis.utils.arcgisUrl = "https://stockland.maps.arcgis.com/sharing/rest/content/items";

    var lods = [
			{
				"level": 1,
				"resolution": 1.058335,
				"scale": 4000
			},
			{
				"level": 2,
				"resolution": 0.714376,
				"scale": 2700
			},
			{
				"level": 3,
				"resolution": 0.529168,
				"scale": 2001
			},
			{
				"level": 4,
				"resolution": 0.396876,
				"scale": 1500
			},
			{
				"level": 5,
				"resolution": 0.317301,
				"scale": 1200
			},
			{
				"level": 6,
				"resolution": 0.238125,
				"scale": 900
			},
			{
				"level": 7,
				"resolution": 0.185209,
				"scale": 700
			},
			{
				"level": 8,
				"resolution": 0.132292,
				"scale": 500
			},
			{
				"level": 9,
				"resolution": 0.092604,
				"scale": 350
			},
			{
				"level": 10,
				"resolution": 0.066146,
				"scale": 250
			},
			{
				"level": 11,
				"resolution": 0.052917,
				"scale": 200
			}
		];


    var map;
    var bookmarks;
    var basemap;
    var mapLayersDic = {};
		var flooredLayers = [];
    var floorNumbers;
    var initialExtent;

    //Constants----------------------
    var storeLayerName = "DEV_Stockland_CP_Retail_Website_WFL1 - StocklandStores";
		var tenantedStoreLayerName = "DEV_Stockland_CP_Retail_Website_WFL1 - StocklandTenantedStores";
    var searchFieldName = "TradingName"; //This field is used in search widget as store name
		var storeKeyField="ExternalKey";
    var levelFieldName = "Level_"; 
    var levelSortFieldName = "LevelSort";
    var storeIdFieldName = "ExternalKey" //This field is used for zoom to a user selected store in store detail map page
    var centreIdFieldName = "BusinessEntity";
    //-------------------------------

    var currentVisibleLevel = "G";
		
		var portalURL = "https://stockland.maps.arcgis.com";
		var mapName = "DEV_Stockland_CP_Retail_Website";
		
		var biHandle=busyIndicator.create("map");
		biHandle.show();
		
		portal=new arcgisPortal.Portal(portalURL);
		
		on(portal, "load", function(p) {
			portal.queryItems({
				num: 1,
				q: "type:'Web Map' AND title:'" + mapName + "'"
			}).then(function(result) {
				if (result.results.length == 0) {
					console.error("Could not find web map named " + mapName);
					biHandle.hide();
				}
				else {
					console.log("Map with id " + result.results[0].id + " will be used");
					esri.arcgis.utils.createMap(result.results[0].id, "map", {
						mapOptions: {
							//backgroundColor: '#dbdddc',
							slider: true, 
							sliderPosition: "bottom-right",
							logo:false,
							lods: lods
						}
					}).then(function (response) {
						map = response.map;

						//This shows the popup window on top of the selected feature
						map.infoWindow.set("anchor", "top");
						//map.infoWindow.set("border", "2px solid #fff;");

						bookmarks = response.itemInfo.itemData.bookmarks;                
						// var layers = response.itemInfo.itemData.operationalLayers;

						getMapLayers();       

						var StoreSapId = "";
						//  var CentreSapId = "3105201";
						//var CentreSapId = "3107801";
						//var CentreSapId = "3102601";
						//var CentreSapId = "3103001";
						if (typeof CentreSapId !== 'undefined')
							ZoomToBookmark(CentreSapId);
							//zoomToAssetBase(CentreSapiId);
						else {
							initialExtent = map.extent;
							console.log("Error zoom to bookmark: CentreSapId is undefined.")
						}
							
						var floorNumbers = getFloorNumbers();

						addHighlightingEffect();

						map.on('extent-change', function (event) {

							if (!initialExtent) {
								//As I have set custom zoom levels the initialExtent dose not set exactly to map extent. so after map.extent is set based on LODs and the 
								//bookmark extent(don't know how exactly) I again set the initilExtent with current map extent
								initialExtent = map.extent;
							}

							var extent = map.extent.getCenter();
							if (initialExtent.contains(extent)) { }
							else { map.setExtent(initialExtent) }
						});

						 
						map.graphics.enableMouseEvents();
						//map.graphics.on("mouse-out", closeDialog);
						// map.infoWindow.set("fillSymbol", new SimpleFillSymbol().setColor(new Color([170, 190, 255, 0.35])));

						//var homeButton = new HomeButton({
						//    theme: "HomeButton",
						//    map: map,
						//    extent: null,
						//    visible: true
						//}, dojo.query(".esriSimpleSliderDecrementButton")[0], "after");
						//homeButton.startup();


						addHomeSlider();

						//Search widget
						var search = new Search({
							map: map,
							allPlaceholder: "Search stores"
						}, "search");

						search.on("load", function () {
							var sources = search.sources;//[];//search.get("sources");
							//sources.clear();
							sources.push({
								featureLayer: mapLayersDic[storeLayerName + "_Search"],
								searchFields: [searchFieldName],
								suggestionTemplate: "${" + searchFieldName + "} - Level ${" + levelFieldName + "}",
								displayField: searchFieldName, 
								placeholder: "Search stores",
								searchExtent: initialExtent,
								exactMatch: false,
								outFields: [searchFieldName, storeKeyField, levelFieldName],
								name: "Store Name",
								//highlightSymbol: symbol,
								maxResults: 6,
								maxSuggestions: 6,
								zoomScale: 6000,
								enableSuggestions: true,
								minCharacters: 0
							});
							sources.splice(0, 1); //Remove the geocoder source            
							search.set("sources", sources);            
							search.activeSource = search.sources[0];
							search.on("select-result", showSearchLocation);            
						});
						search.startup();
						biHandle.hide();
					}, function (error) {
						console.log("Error: ", error.code, " Message: ", error.message);
						biHandle.hide();
					});
				}
			});
		});


    function addHomeSlider() {

        //let's add the home button slider as a created class, requrires dom-Attr  
        dojo.create("div", {
            className: "esriSimpleSliderHomeButton",
            title: 'Zoom to Full Extent',
            onclick: function () {
                if (initialExtent === undefined) {
                    initialExtent = map.extent;
                }
								var inp=document.getElementById("G");
								inp.checked=true;
                changeLevelsState(inp);
                map.setExtent(initialExtent);
            }
        }, dojo.query(".esriSimpleSliderIncrementButton")[0], "after");
    }

    function getMapLayers() {  
			arrayUtils.forEach(map.graphicsLayerIds, function(layerId) {
				var layer = map.getLayer(layerId);
				if (layerId != "labels") {
					mapLayersDic[layer.arcgisProps.title]=layer;
				}
				if (layer.visible) {
					var levelFields=arrayUtils.filter(layer.fields, function(field) {
						return field.name==levelFieldName;
					});
					if (levelFields.length > 0) {
						flooredLayers.push(layer);
					}
				}
			});
    }
    
    function ZoomToBookmark(bookmarkName) {
			var bookmark = arrayUtils.filter(bookmarks, function(bookmark) {
				return bookmark.name==bookmarkName;
			});
			if (bookmark.length > 0) {
        initialExtent = new esri.geometry.Extent(bookmark[0].extent);
				map.setExtent(initialExtent);
			}
			else {
        initialExtent = map.extent;
        console.log("Error zoom to bookmark: CentreSapId = " + bookmarkName + " is invalid.");
			}
    }
		
    function zoomToAssetBase(CentreSapId) {
        var assetLayer = mapLayersDic["StocklandRetailCentres_WFL1 - StocklandAssetBase"];
        var query = new Query();
        //query.where = "";  // query for non-null values  
        query.returnGeometry = true;          // turn geometry off, required to be false when using returnDistinctValues  
        query.where = "AssetID = '" + CentreSapId  + "'";

        var queryTask = new QueryTask(assetLayer.url);
        queryTask.execute(query, function (featureset) {
            asset = featureset.features[0];
            initialExtent = asset.geometry.getExtent().expand(1.2);
            map.setExtent(initialExtent);
        });
    }

    function getFloorNumbers() {
			var query = new Query();
			query.returnGeometry = false;          // turn geometry off, required to be false when using returnDistinctValues  
			query.returnDistinctValues = true;
			query.outFields = [levelFieldName];
			if (typeof CentreSapId !== 'undefined') {
				query.where = centreIdFieldName + "='" + CentreSapId + "'";
			}
			else {
				query.spatialRelationship = esri.tasks.Query.SPATIAL_REL_CONTAINS;
				query.geometry = initialExtent;//using map.extent caused problem. Seems the initialExtent has not been set to map yet
			}
			
			var floors = [];
			var defs = [];
			arrayUtils.forEach(flooredLayers, function(flooredLayer) {
        var queryTask = new QueryTask(flooredLayer.url);
        defs.push(queryTask.execute(query, function (featureset) {
					arrayUtils.forEach(featureset.features, function(feature) {
						var floor = feature.attributes[levelFieldName];
						if (floors.indexOf(floor) == -1) {
							floors.push(floor);
						}
					});
				}, function(error) {
					console.error("Query failed against " + flooredLayer.name + " with error:" + error);
				}));
			});

			all(defs).then(function() {
				if (floors.length <= 1) return;

				floors.sort(function(a,b) {return (a == "G" ? 0 : a) - (b == "G" ? 0 : b)});
				var zoomSlider = document.getElementById("map_zoom_slider");

				arrayUtils.forEach(floors, function(floor) {
					var lbl = document.createElement("label");
					lbl.innerHTML = floor;
					lbl.setAttribute("Class", "input-check");


					var input = document.createElement("input");
					input.setAttribute("Type", "checkbox");
					input.setAttribute("ID", floor);
					//input.setAttribute("onchange", "changeLevelsState(this)");
					input.onchange = changeLevelsState;

					//zoomSlider.appendChild(lbl);

					zoomSlider.insertBefore(lbl, zoomSlider.childNodes[0]);

					lbl.appendChild(input);
					if (floor == "G") {
						lbl.classList.add("checked");
						input.checked = true;
						changeLevelsState(input);
					}

					var br = document.createElement("br");
				});
			});
    }

    function changeLevelsState(obj) {
        if (obj.target)
            obj = obj.target;
				map.infoWindow.hide();
        if (obj.checked) {
            //Change the state of current visible lavel to unchecked
            if (obj.getAttribute("ID") != currentVisibleLevel) {
                document.getElementById(currentVisibleLevel).checked = false;
                changeLevelsState(document.getElementById(currentVisibleLevel));
                currentVisibleLevel = obj.getAttribute("ID");
            }

            //if checkbox is being checked, add a "checked" class
            obj.parentNode.classList.add("checked");

            //trun the level on
						arrayUtils.forEach(flooredLayers, function(flooredLayer) {
							flooredLayer.setDefinitionExpression(levelFieldName + " = '" + obj.getAttribute("ID") + "'");
						});
        }
        else {
            //else remove it
            obj.parentNode.classList.remove("checked");
            //turn the level off
        }
    }

    function showSearchLocation(evt) {
        map.graphics.clear();

        //Find this store level button element, trun it on and change the button state to checked
        if (document.getElementById(evt.result.feature.attributes[levelFieldName])) {
            levelButton = document.getElementById(evt.result.feature.attributes[levelFieldName]);
            levelButton.checked = true;
            changeLevelsState(levelButton);
        }
        var sapid = evt.result.feature.attributes[storeKeyField];
        var centerPoint = evt.result.extent.getCenter();
        var loc = map.toScreen(centerPoint);
        map.infoWindow.set("fillSymbol", null);
        map.infoWindow.set("anchor", "top");

        var highlightSymbol = new SimpleFillSymbol(
            SimpleFillSymbol.STYLE_SOLID,
            null,
            new Color([170, 190, 255, 0.35])
       );

        getStoreDetails(sapid,
               function (response) {
                   if (response == "") //No responce from the callout
                       response = '{"StoreName": "' + evt.result.feature.attributes[searchFieldName] + '" }';
                   var content = createPopupContent(response);
                   map.infoWindow.setTitle(" ");
                   map.infoWindow.setContent(content);
                   map.infoWindow.show(loc);

                   var highlightGraphic = new Graphic(evt.result.feature.geometry, highlightSymbol);
                   map.graphics.add(highlightGraphic);

               });
    }

    function addHighlightingEffect() {
        var storeLayer = mapLayersDic[tenantedStoreLayerName];

        var highlightSymbol = new SimpleFillSymbol(
         SimpleFillSymbol.STYLE_SOLID,
         null,
         new Color([170, 190, 255, 0.35])
        );

        var highlightedStore;
        storeLayer.on("mouse-over", function (evt) {
            map.graphics.clear();
            map.setMapCursor("pointer");
            var highlightGraphic = new Graphic(evt.graphic.geometry, highlightSymbol);
            map.graphics.add(highlightGraphic);
            highlightedStore = evt.graphic;
        });

        map.graphics.on("mouse-out", function (evt) {
            map.graphics.clear();
            map.setMapCursor("default");
        });

        map.graphics.on("click", function (evt) {
            if (!highlightedStore)
                return;
            getStoreDetails(highlightedStore.attributes[storeKeyField],
               function (response) {
                   if (response == "") //No responce from the callout
                       response = '{"StoreName": "' + highlightedStore.attributes[searchFieldName] + '" }';
											 
                   var infoTemplate = new InfoTemplate({ anchor: "top" });
                   infoTemplate.setTitle(" ");
                   infoTemplate.setContent(createPopupContent(response));
                   evt.graphic.setInfoTemplate(infoTemplate);
               });
        });
    }

    function createPopupContent(response) {
        var storeInfoArr = JSON.parse(response);
        var content;
        if (Object.keys(storeInfoArr).length == 1) {
            //Showing just the store name
            var storename = storeInfoArr.StoreName;
            content = '<div class="popup">' +
                      '<div class="store-name">' + storename + '</div>' +
                      '</div>';
            return content;
        }

        var storename = storeInfoArr.StoreName;
        var storeUrl = storeInfoArr.StorePageUrl;
        var storeLogo = storeInfoArr.StoreLogoUrl;
        var openingHours = storeInfoArr.OpeningHours;
        var phoneNumber = storeInfoArr.PhoneNumber;
        var level = storeInfoArr.Level;
        var storePageUrl = storeInfoArr.StorePageUrl;

        //Create popup html content
        content = "<a  href =" + "'" + storePageUrl + "'" + ">" +
            '<div class="popup"><table border="0"><tr><td class="store-name" colspan="2">' + storename + '</td></tr>' +
            '<tr><td class="store-opening-hours">Hours: </td><td class="store-opening-hours">' + openingHours + '</td></tr>' +
            '<tr><td class="phone-number">Phone: </td><td class="phone-number">' + phoneNumber + '</td></tr>' +
            '<tr><td class="store-level">Level: </td><td class="store-level">' + level + '</td></tr></table></div>' +
            "</a>";

        return content;
    }

    //function closeDialog() {
    //    map.graphics.clear();
    //    dijitPopup.close(dialog);
    //}

    function getStoreDetails(sapId, callback) {
        if (sapId != '' && sapId != null) {
            $.ajax({
                url: "/public/map/GetRetailStoreDetails",
                method: "POST",
                contentType: 'application/json; charset=utf-8',
                data: "{'storeSapId':" + JSON.stringify(sapId) + "}",
                success: function (response) {
                    callback(response);
                },
                error: function (exception) {
                    console.log('Could not retrieve Store details : ' + exception.statusText);
                    callback("");
                }
            });
        } else {
            console.log("SapId is empty");
            callback("");
        }
    }
});

//change the color of polygon graphics returned by a successful search. 
//this.map.infoWindow.set("fillSymbol", new SimpleFillSymbol().setColor(new esriColor([0,0,0,.4])));